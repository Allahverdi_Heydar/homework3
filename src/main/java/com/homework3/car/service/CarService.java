package com.homework3.car.service;

import com.homework3.car.dto.CarDto;
import com.homework3.car.dto.CreateCarDto;
import com.homework3.car.dto.UpdateCarDto;
import com.homework3.car.entity.Car;
import com.homework3.car.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarService {

    private final CarRepository carRepository;
    private  final ModelMapper modelMapper;


    public CarService(CarRepository carRepository, ModelMapper modelMapper) {
        this.carRepository = carRepository;
        this.modelMapper = modelMapper;
    }


    public List<CarDto> getAll() {
        List<Car> cars = carRepository.findAll();
        List<CarDto> getCarDtoList = cars.stream().map(
                        (car) -> modelMapper
                                .map(car, CarDto.class))
                .collect(Collectors.toList());
        return getCarDtoList;
    }

    public CarDto getById(Integer id) {
        Car car = carRepository.findById(id).get();
        CarDto carDto = modelMapper.map(car, CarDto.class);
        return carDto;
    }

    public void create(CreateCarDto carDto) {
        Car car = modelMapper.map(carDto, Car.class);
        carRepository.save(car);
    }

    public void update(Integer id, UpdateCarDto updateCarDto) {
        Car car = carRepository.findById(id).get();
        car.setColor(updateCarDto.getColor());
        car.setEngine(updateCarDto.getEngine());
        car.setModel(updateCarDto.getModel());
        car.setMaker(updateCarDto.getMaker());
        car.setYear(updateCarDto.getYear());
        carRepository.save(car);
    }

    public void deleteById(Integer id) {
        carRepository.deleteById(id);
    }


    public void deleteAll() {
        carRepository.deleteAll();
    }






}
