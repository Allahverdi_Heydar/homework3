package com.homework3.car.controller;

import com.homework3.car.dto.CarDto;
import com.homework3.car.dto.CreateCarDto;
import com.homework3.car.dto.UpdateCarDto;
import com.homework3.car.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {
    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/all")
    public List<CarDto> getAllCars(){
        return carService.getAll();
    }

    @GetMapping("/{id}")
    public CarDto getCarById(@PathVariable Integer id){
        return carService.getById(id);
    }

    @PostMapping
    public void createCar(@RequestBody CreateCarDto carDto){
        carService.create(carDto);
    }

    @PutMapping("/updatecar/{id}")
    public void deleteById(@PathVariable Integer id, @RequestBody UpdateCarDto updateCarDto){
        carService.update(id, updateCarDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Integer id){
        carService.deleteById(id);
    }

    @DeleteMapping("/all")
    public void deleteAllCars(){
        carService.deleteAll();
    }
}
