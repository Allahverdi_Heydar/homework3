package com.homework3.car.dto;

import lombok.Data;

@Data
public class CarDto {
    private Integer id;
    private String color;
    private  String engine;
    private  String model;
    private  String maker;
    private  Integer year;

}
